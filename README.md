# Campus Sampoerna

>Front-End Design Web Development of Campus Sampoerna

## Environment

#### Table of content

- [Landing Page](http://#) - Landing Page / Main Page


#### Dependencies

JS Library
- [jQuery](https://jquery.com) - jQuery

CSS Library
- [Normalize.css](https://necolas.github.io/normalize.css/) - A modern, HTML5-ready alternative to CSS resets
- [Reset](http://mebyerweb.com/eric/tools/css/reset/) - The goal of a reset stylesheet is to reduce browser inconsistencies in things like default line heights, margins and font sizes of headings, and so on.

## Installation

1. Install [Node.js](http://nodejs.org/) - 4.0.0 or above
2. Install [npm](http://npmjs.com/)
    ```sh
    $ npm install npm -g
    ```
3. Install [bower](http://bower.io/)
    ```sh
    $ npm install bower -g
    ```
4. Install [gulp](http://gulpjs.com/)
    ```s
    $ npm install gulp-cli -g
    ```
    
## Team

- **Asep Irman**

## History

- **V 1.0**
    Landing Page.

## License

MIT License
Copyright (C) 2020 Asep Irman, https://dribbble.com/sibaturaspirman/